import Axios from "axios";
const api = Axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com',
    headers: {
        'Content-Type': ' application/json',
        'Accept': 'application/json',
    }
});

export default api; 