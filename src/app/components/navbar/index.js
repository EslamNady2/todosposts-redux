import React from 'react';
import { NavLink } from 'react-router-dom'
function Navbar() {
    return (
        <React.Fragment>
            <div className='py-2 bg-white'>
                <nav className='navbar justify-content-center'>
                    <div className='nav-link-wrapper d-flex'>
                        <NavLink to='/todos' className='nav-link font-medium weight-600'>Todo</NavLink>
                        <NavLink to='/posts' className='nav-link font-medium weight-600'>Posts</NavLink>
                    </div>
                </nav>
            </div>
        </React.Fragment>
    )
}
export default Navbar;