import React, { Component } from 'react';
//styles
import '../sass/app.sass'
//antd style
import 'antd/dist/antd.css'

//react router
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

//redux
import { Provider } from 'react-redux'
import store from './redux/store'

//Components
import Navbar from './components/navbar'
//views
import PostsPage from './views/posts'
import TodosPage from './views/todos'
class App extends Component {

  render() {
    return (
      <React.Fragment>
        <Provider store={store}>
          <Router>
            <header>
              <Navbar />
            </header>
            <main>
              <Switch>
                <Route path={`/todos`} render={(props) => <TodosPage {...props} />} />
                <Route path={`/posts`} render={(props) => <PostsPage {...props} />} />
                <Route render={(props) => <Redirect to={`/todos`} />} />
              </Switch>
            </main>
          </Router>
        </Provider>
      </React.Fragment>
    );
  }
}

export default App;