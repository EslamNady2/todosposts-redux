import React, { Component } from 'react';
import { Checkbox } from 'antd';
import { LoadingOutlined } from '@ant-design/icons'
import { connect } from 'react-redux'
import { updateTodo } from '../../redux/actions/todoActions'
class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    changeTodoStatus = (e) => {
        const { updateTodo, todo } = this.props
        this.setState({ is_loading: true })
        updateTodo({ ...todo, completed: e.target.checked })
    }


    render() {
        const { todo } = this.props
        return (
            <React.Fragment>
                <li className={`todo-list-item ${todo.completed ? 'comeleted' : ''}`}>
                    <label className='todo-list-item-label'>
                        <div className='id-wrapper weight-700 font-Lsmall mx-3'><span>{todo.id}</span></div>
                        <div className='title-wrapper weight-600 font-Lsmall'>
                            <span>{todo.title}</span>
                        </div>
                        <div className='checkbox-wrapper mx-2'>
                            {
                                !todo.is_loading ?
                                    <Checkbox checked={todo.completed} onChange={this.changeTodoStatus} />
                                    : <span className='text-theme-color'> <LoadingOutlined /></span>
                            }
                        </div>
                    </label>

                </li>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    todos: state.todos.items
})
export default connect(mapStateToProps, { updateTodo })(Todo);