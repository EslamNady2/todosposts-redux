import React, { Component } from 'react';
import TodosList from './todos-list'
class TodosPage extends Component {
    constructor(props) {
        super(props);
        document.title = 'To-do'
    }
    render() {
        return (
            <React.Fragment>
                <div className='row mx-auto py-4'>
                    <div className='col-md-8 col-12 mx-auto'>
                        <TodosList />
                    </div>
                </div>

            </React.Fragment>
        );
    }
}

export default TodosPage;