import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchTodos } from '../../redux/actions/todoActions'
import PaginationList from '../../components/pagination-list'

import Todo from './todo';
class TodosList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current_page: 1,
        }
    }
    componentWillMount() {
        const { fetchTodos, todos } = this.props
        if (todos.length == 0) //prevent re-fetching data because the updates we did on data is not actually saved on the server 
            fetchTodos()

    }
    changePage = (new_page) => {
        this.setState({ current_page: new_page })
    }

    render() {
        const { todos } = this.props
        const { current_page } = this.state
        return (
            <React.Fragment>
                <div className='todos-list-wrapper'>
                    <ul className='todo-list'>
                        {
                            todos.slice((10 * (current_page - 1)), (10 * (current_page - 1)) + 10).map((todo) => (
                                <Todo todo={todo} key={todo.id} />
                            ))
                        }
                    </ul>
                </div>

                <div className='pagination-list-wrapper'>
                    <PaginationList total={Math.ceil(todos.length / 10)} current_page={current_page} onChange={this.changePage} />
                </div>
            </React.Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    todos: state.todos.items
})
export default connect(mapStateToProps, { fetchTodos })(TodosList);