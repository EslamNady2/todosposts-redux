import React, { Component } from 'react';
class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        const { post } = this.props
        return (
            <React.Fragment>

                <li className='posts-list-item my-2'>
                    {/* <div className='post-id'>{post.id}</div> */}
                    <div className='post-title font-medium weight-600 text-theme-color mb-2'>{post.title}</div>
                    <div className='post-body font-Lsmall weight-600 text-gray-500 px-1'>{post.body}</div>
                </li>
            </React.Fragment>
        );
    }
}

export default Post;