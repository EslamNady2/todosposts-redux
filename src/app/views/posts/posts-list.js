import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchPosts } from '../../redux/actions/postActions'
import PaginationList from '../../components/pagination-list'

import Post from './post'
class PostsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current_page: 1,
        }
    }
    componentWillMount() {
        const { fetchPosts, posts } = this.props
        if (posts.length == 0) //prevent re-fetching data because the updates we did on data is not actually saved on the server 
            fetchPosts()
    }
    changePage = (new_page) => {
        this.setState({ current_page: new_page })
    }
    render() {
        const { posts } = this.props
        const { current_page } = this.state
        return (
            <React.Fragment>
                <div className='posts-list-wrapper'>
                    <ul className='posts-list'>
                        {
                            posts.slice((10 * (current_page - 1)), (10 * (current_page - 1)) + 10).map((post, index) => (
                                <Post post={post} key={index} />
                            ))
                        }
                    </ul>
                </div>
                <div className='pagination-list-wrapper'>
                    <PaginationList total={Math.ceil(posts.length / 10)} current_page={current_page} onChange={this.changePage} />
                </div>
            </React.Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    posts: state.posts.items,
})

export default connect(mapStateToProps, { fetchPosts })(PostsList);