import React, { Component } from 'react';
import { Input } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { connect } from 'react-redux'
import { createPost } from '../../redux/actions/postActions'
class AddPostForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            body: '',
        }
    }
    updateInput = (event) => {
        const { target } = event
        const { name, value } = target
        this.setState({ [name]: value })
    }
    isBlank = (str) => {
        return (!str || /^\s*$/.test(str));
    }
    submit = (event) => {
        event.preventDefault()
        const post = {
            title: this.state.title,
            body: this.state.body,
        }
        if (!this.isBlank(post.title) && !this.isBlank(post.body)) {
            this.setState({ is_loading: true })
            this.props.createPost(post)
        }

    }
    UNSAFE_componentWillReceiveProps(newProp) {
        if (this.props.is_loading == true && newProp.is_loading == false) {
            this.setState({
                title: '',
                body: '',
            })
        }
    }
    render() {
        const { is_loading } = this.props
        return (
            <React.Fragment>
                <div className='add-post-form-wrapper form-wrapper'>
                    <form className='form' onSubmit={this.submit}>
                        <div className='form-input-wrapper'>
                            <div className='input-wrapper'>
                                <Input className='form-input' placeholder='Title' name='title' value={this.state.title} onChange={this.updateInput} disabled={is_loading} />
                            </div>
                        </div>
                        <div className='form-input-wrapper'>
                            <div className='input-wrapper'>
                                <Input.TextArea style={{ height: '100px' }} className='form-input' placeholder='body' name='body' value={this.state.body} onChange={this.updateInput} disabled={is_loading} />
                            </div>
                        </div>
                        <div className='btn-wrapper d-flex justify-content-end'>
                            <button className='btn btn-primary font-Lsmall weight-600 px-5' disabled={is_loading}>
                                {
                                    !is_loading ?
                                        'Post'
                                        : <LoadingOutlined />
                                }
                            </button>
                        </div>
                    </form>
                </div>
            </React.Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    is_loading: state.posts.is_loading,
})

export default connect(mapStateToProps, { createPost })(AddPostForm);