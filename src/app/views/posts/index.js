import React, { Component } from 'react';
import PostsList from './posts-list'
import AddNewPostForm from './add-post-form'
class PostsPage extends Component {
    constructor(props) {
        super(props);
        document.title = 'Posts'
    }
    render() {
        return (
            <React.Fragment>
                <div className='row mx-auto py-4'>
                    <div className='col-md-8 col-12 mx-auto'>
                        {/* add post form component */}
                        <AddNewPostForm />
                        {/* posts list*/}
                        <PostsList />

                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default PostsPage;