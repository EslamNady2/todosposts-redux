import { FETCH_POST, NEW_POST, POSTING } from '../actions/types'

const initial_state = {
    items: [],
    is_loading: false
}

export default function (state = initial_state, action) {
    switch (action.type) {

        case FETCH_POST:
            return {
                ...state,
                items: action.payload
            }
        case POSTING:
            return {
                ...state,
                is_loading: action.payload
            }
        case NEW_POST:
            return {
                ...state,
                items: [action.payload, ...state.items],
                is_loading: false
            }

        default:
            return state
    }
}