import { FETCH_TODO, UPDATE_TODO } from '../actions/types'

const initial_state = {
    items: [],
}

export default function (state = initial_state, action) {
    switch (action.type) {

        case FETCH_TODO:
            return {
                ...state,
                items: action.payload,
            }
        case UPDATE_TODO:
            console.log(action.payload)
            return {
                ...state,
                // items: state.items
                items: state.items.map(item => {
                    if (item.id == action.payload.id) {
                        item = { ...action.payload }
                    }
                    return item
                })
            }


        default:
            return state
    }
}