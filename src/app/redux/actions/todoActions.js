import { FETCH_TODO, UPDATE_TODO } from './types'
import axios from '../../helperObjects/axios'
export const fetchTodos = () => (dispatch) => {
    axios({
        method: 'get',
        url: `/todos`,
    }
    ).then((res) => {
        const data = res.data.map(todo => ({ ...todo, is_loading: false }))
        dispatch({
            type: FETCH_TODO,
            payload: data
        })
        console.log("RESPONSE RECEIVED: ", res);
    }).catch((err) => {

        console.log("AXIOS ERROR: ", err);
    })

}
export const updateTodo = (todo) => (dispatch) => {
    dispatch({
        type: UPDATE_TODO,
        payload: { ...todo, is_loading: true, completed: !todo.completed }
    })
    axios({
        method: 'put',
        url: `/todos/${todo.id}`,
        data: todo
    }
    ).then((res) => {
        console.log(res.data)
        dispatch({
            type: UPDATE_TODO,
            payload: { ...res.data, is_loading: false }
        })
        console.log("RESPONSE RECEIVED: ", res);
    }).catch((err) => {
        console.log("AXIOS ERROR: ", err);
    })

}