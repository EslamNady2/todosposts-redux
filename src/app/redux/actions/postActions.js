import { FETCH_POST, NEW_POST, POSTING } from './types'
import axios from '../../helperObjects/axios'

export const fetchPosts = () => (dispatch) => {
    axios({
        method: 'get',
        url: `/posts`,
    }
    ).then((res) => {
        dispatch({
            type: FETCH_POST,
            payload: res.data,
        })
        console.log("RESPONSE RECEIVED: ", res);
    }).catch((err) => {
        console.log("AXIOS ERROR: ", err);
    })

}
export const createPost = (post) => (dispatch) => {
    dispatch({
        type: POSTING,
        payload: true
    })
    axios({
        method: 'post',
        url: `/posts`,
        data: {
            title: post.title,
            body: post.body
        }

    }
    ).then((res) => {
        dispatch({
            type: NEW_POST,
            payload: res.data
        })
        console.log("RESPONSE RECEIVED: ", res);
    }).catch((err) => {

        console.log("AXIOS ERROR: ", err);
    })

}